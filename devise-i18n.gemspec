#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: devise-i18n 1.8.0 ruby lib

Gem::Specification.new do |s|
  s.name = "devise-i18n".freeze
  s.version = "1.8.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Christopher Dell".freeze, "mcasimir".freeze, "Jason Barnabe".freeze]
  s.date = "2019-02-17"
  s.description = "Translations for the devise gem".freeze
  s.email = "chris@tigrish.com".freeze
  s.extra_rdoc_files = ["LICENSE.txt".freeze, "README.md".freeze]
  s.files = ["LICENSE.txt".freeze, "README.md".freeze, "VERSION".freeze, "app/views/devise/confirmations/new.html.erb".freeze, "app/views/devise/mailer/confirmation_instructions.html.erb".freeze, "app/views/devise/mailer/email_changed.html.erb".freeze, "app/views/devise/mailer/password_change.html.erb".freeze, "app/views/devise/mailer/reset_password_instructions.html.erb".freeze, "app/views/devise/mailer/unlock_instructions.html.erb".freeze, "app/views/devise/passwords/edit.html.erb".freeze, "app/views/devise/passwords/new.html.erb".freeze, "app/views/devise/registrations/edit.html.erb".freeze, "app/views/devise/registrations/new.html.erb".freeze, "app/views/devise/sessions/new.html.erb".freeze, "app/views/devise/shared/_error_messages.html.erb".freeze, "app/views/devise/shared/_links.html.erb".freeze, "app/views/devise/unlocks/new.html.erb".freeze, "lib/devise-i18n.rb".freeze, "lib/devise-i18n/railtie.rb".freeze, "lib/generators/devise/i18n/locale_generator.rb".freeze, "lib/generators/devise/i18n/views_generator.rb".freeze, "lib/generators/devise/templates/simple_form_for/confirmations/new.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/passwords/edit.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/passwords/new.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/registrations/edit.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/registrations/new.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/sessions/new.html.erb".freeze, "lib/generators/devise/templates/simple_form_for/unlocks/new.html.erb".freeze, "rails/locales/af.yml".freeze, "rails/locales/ar.yml".freeze, "rails/locales/az.yml".freeze, "rails/locales/be.yml".freeze, "rails/locales/bg.yml".freeze, "rails/locales/bn.yml".freeze, "rails/locales/bs.yml".freeze, "rails/locales/ca.yml".freeze, "rails/locales/cs.yml".freeze, "rails/locales/da.yml".freeze, "rails/locales/de-CH.yml".freeze, "rails/locales/de.yml".freeze, "rails/locales/el.yml".freeze, "rails/locales/en-GB.yml".freeze, "rails/locales/en.yml".freeze, "rails/locales/es-MX.yml".freeze, "rails/locales/es.yml".freeze, "rails/locales/et.yml".freeze, "rails/locales/fa.yml".freeze, "rails/locales/fi.yml".freeze, "rails/locales/fr-CA.yml".freeze, "rails/locales/fr.yml".freeze, "rails/locales/ha.yml".freeze, "rails/locales/he.yml".freeze, "rails/locales/hi.yml".freeze, "rails/locales/hr.yml".freeze, "rails/locales/hu.yml".freeze, "rails/locales/id.yml".freeze, "rails/locales/ig.yml".freeze, "rails/locales/is.yml".freeze, "rails/locales/it.yml".freeze, "rails/locales/ja.yml".freeze, "rails/locales/ka.yml".freeze, "rails/locales/ko.yml".freeze, "rails/locales/lo-LA.yml".freeze, "rails/locales/lt.yml".freeze, "rails/locales/lv.yml".freeze, "rails/locales/ms.yml".freeze, "rails/locales/nb.yml".freeze, "rails/locales/nl.yml".freeze, "rails/locales/nn-NO.yml".freeze, "rails/locales/no.yml".freeze, "rails/locales/pap-AW.yml".freeze, "rails/locales/pap-CW.yml".freeze, "rails/locales/pl.yml".freeze, "rails/locales/pt-BR.yml".freeze, "rails/locales/pt.yml".freeze, "rails/locales/ro.yml".freeze, "rails/locales/ru.yml".freeze, "rails/locales/si.yml".freeze, "rails/locales/sk.yml".freeze, "rails/locales/sl.yml".freeze, "rails/locales/sq.yml".freeze, "rails/locales/sr-RS.yml".freeze, "rails/locales/sr.yml".freeze, "rails/locales/sv.yml".freeze, "rails/locales/th.yml".freeze, "rails/locales/tl.yml".freeze, "rails/locales/tr.yml".freeze, "rails/locales/uk.yml".freeze, "rails/locales/ur.yml".freeze, "rails/locales/vi.yml".freeze, "rails/locales/yo.yml".freeze, "rails/locales/zh-CN.yml".freeze, "rails/locales/zh-HK.yml".freeze, "rails/locales/zh-TW.yml".freeze, "rails/locales/zh-YUE.yml".freeze]
  s.homepage = "http://github.com/tigrish/devise-i18n".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "Translations for the devise gem".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<activemodel>.freeze, [">= 0"])
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.2"])
      s.add_runtime_dependency(%q<devise>.freeze, [">= 4.6"])
      s.add_development_dependency(%q<i18n-spec>.freeze, ["~> 0.6.0"])
      s.add_development_dependency(%q<jeweler>.freeze, ["> 1.6.4"])
      s.add_development_dependency(%q<localeapp>.freeze, [">= 0"])
      s.add_development_dependency(%q<railties>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, [">= 2.8.0"])
      s.add_development_dependency(%q<rspec-rails>.freeze, [">= 0"])
    else
      s.add_dependency(%q<activemodel>.freeze, [">= 0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.2"])
      s.add_dependency(%q<devise>.freeze, [">= 4.6"])
      s.add_dependency(%q<i18n-spec>.freeze, ["~> 0.6.0"])
      s.add_dependency(%q<jeweler>.freeze, ["> 1.6.4"])
      s.add_dependency(%q<localeapp>.freeze, [">= 0"])
      s.add_dependency(%q<railties>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, [">= 2.8.0"])
      s.add_dependency(%q<rspec-rails>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<activemodel>.freeze, [">= 0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.2"])
    s.add_dependency(%q<devise>.freeze, [">= 4.6"])
    s.add_dependency(%q<i18n-spec>.freeze, ["~> 0.6.0"])
    s.add_dependency(%q<jeweler>.freeze, ["> 1.6.4"])
    s.add_dependency(%q<localeapp>.freeze, [">= 0"])
    s.add_dependency(%q<railties>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, [">= 2.8.0"])
    s.add_dependency(%q<rspec-rails>.freeze, [">= 0"])
  end
end
